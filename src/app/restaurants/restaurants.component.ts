import { Component, OnInit } from '@angular/core';
import { Restaurant } from './restaurant/restaurant.model';
import { RestaurantsService } from './restaurants.service';

import { trigger, state, transition, style, animate } from '@angular/animations';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { switchMap, debounceTime, distinctUntilChanged, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
  animations: [
    trigger('toggleSearch', [
      state('hidden', style({
        opacity: 0,
        "max-height": "0px"
      })),
      state('visible', style({
        opacity: 1,
        "max-height": "70px",
        "margin-top": "20px"
      })),
      transition('* => *', animate('250ms 0s ease-in-out'))
    ])
  ]
})
export class RestaurantsComponent implements OnInit {

  searchBarState = 'hidden'
  searchBarFocus = false
  restaurants: Restaurant[]

  searchForm: FormGroup
  searchControl: FormControl

  constructor(private restaurantsService: RestaurantsService, private fb: FormBuilder) { }

  /*Executa somente 1 vez, ao iniciar o componente */
  ngOnInit() {

    this.searchControl = this.fb.control('')
    this.searchForm = this.fb.group({
      searchControl: this.searchControl
    })

    this.searchControl.valueChanges.pipe(
      debounceTime(500) /* esperar meio segundo para chamar a query */
    ).pipe(
      distinctUntilChanged() /* so chamar a query se realmente teve mudanca no texto */
    ).pipe(
      switchMap(searchTerm => 
        this.restaurantsService.restaurants(searchTerm)
        .pipe(catchError(() => {return []})) /* evita quebrar o componente ao dar erro na query */
      ) /* executa a query */
    ).subscribe(restaurants => this.restaurants = restaurants) /* pega o resultado da query e joga pra variavel */

    /*Recebe o Observable e faz o subscribe usando LAMBDA */
    this.restaurantsService.restaurants().subscribe(restaurants => this.restaurants = restaurants)
  }

  toggleSearch() {
    this.searchBarState = this.searchBarState === 'hidden' ? 'visible' : 'hidden'
  }

}
