import { CartItem } from "./cart-item.model";
import { MenuItem } from "../menu-item/menu-item.model";
import { Injectable } from "@angular/core";
import { NotificationService } from "../../shared/messages/notification.service";

@Injectable()
export class ShoppingCartService {
    items: CartItem[] = []

    constructor(private notificationService: NotificationService){}

    clear() {
        this.items = []
    }

    addItem(item: MenuItem) {
        /*verifica se o item ja existe (pelo ID) usando lambda */
        let foundItem = this.items.find((mItem) => mItem.menuItem.id === item.id)
        if (foundItem) {
            this.increaseQty(foundItem)
        } else {
            this.items.push(new CartItem(item))
        }
        this.notificationService.notify(`Você adicionou o item ${item.name}`)
    }

    increaseQty(item: CartItem) {
        item.quantity = item.quantity + 1
    }

    decreaseQty(item: CartItem) {
        item.quantity = item.quantity - 1
        if (item.quantity === 0) {
            this.removeItem(item)
        }
    }

    removeItem(item: CartItem) {
        /*remove 1 item a partir do index deste item */
        this.items.splice(this.items.indexOf(item), 1)
        this.notificationService.notify(`Você removeu o item ${item.menuItem.name}`)
    }

    total(): number {
        /*totaliza os itens, pegando o array de valores e somando eles */
        /*o MAP troca a lista de CartItem para uma lista de VALORES */
        /*o ZERO eh o valor inicial do reduce */
        return this.items.map(item => item.value()).reduce((prev, value) => prev + value, 0)
    }

}