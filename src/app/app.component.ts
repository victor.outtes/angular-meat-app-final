import { Component } from '@angular/core';

@Component({
  selector: 'mt-app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  content = 'Welcome to Meat App!'
}
